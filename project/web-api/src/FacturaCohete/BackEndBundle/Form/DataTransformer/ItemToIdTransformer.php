<?php
/**
 * Created by luiscarlos@sourcequarter.com.
 * User: berliner
 * Date: 13/12/14
 * Time: 21:53
 */

namespace FacturaCohete\BackEndBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use FacturaCohete\BackEndBundle\Entity\Item;

class ItemToIdTransformer implements DataTransformerInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $om;

    /**
     * @param \Doctrine\ORM\EntityManager $om
     */
    public function __construct($om)
    {
        $this->om = $om;
    }
    /**
     * Transforms an object (item) to a integer (id).
     *
     * @param  Item|null $item
     * @return integer
     */
    public function transform($item)
    {


        if (null === $item) {
            return "";
        }

        return $item->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  integer $id
     *
     * @return Item|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        /** @var \FacturaCohete\BackEndBundle\Entity\Item $item */
        $item = $this->om
            ->getRepository('FacturaCoheteBackEndBundle:Item')
            ->find($id);

        if (null === $item) {
            throw new TransformationFailedException(sprintf(
                'An item with id "%s" does not exist!',
                $id
            ));
        }

        return $item;
    }
} 