<?php
/**
 * Created by luiscarlos@sourcequarter.com.
 * User: berliner
 * Date: 13/12/14
 * Time: 21:53
 */

namespace FacturaCohete\BackEndBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use FacturaCohete\BackEndBundle\Entity\Client;

class ClientToIdTransformer implements DataTransformerInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $om;

    /**
     * @param \Doctrine\ORM\EntityManager $om
     */
    public function __construct($om)
    {
        $this->om = $om;
    }
    /**
     * Transforms an object (client) to a integer (id).
     *
     * @param  Client|null $client
     * @return integer
     */
    public function transform($client)
    {


        if (null === $client) {
            return "";
        }

        return $client->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  integer $id
     *
     * @return Client|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        /** @var \FacturaCohete\BackEndBundle\Entity\Client $client */
        $client = $this->om
            ->getRepository('FacturaCoheteBackEndBundle:Client')
            ->find($id);

        if (null === $client) {
            throw new TransformationFailedException(sprintf(
                'An client with id "%s" does not exist!',
                $id
            ));
        }

        return $client;
    }
} 