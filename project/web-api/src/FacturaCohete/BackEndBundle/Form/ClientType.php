<?php

namespace FacturaCohete\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organizationName')
            ->add('country')
            ->add('street')
            ->add('city')
            ->add('province')
            ->add('postalCode')
            ->add('businessPhone')
            ->add('internalNotes')
            ->add('clientContacts', 'collection', array(
                'type' => new ClientContactType(), 'allow_add' => true,'allow_delete' => true
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FacturaCohete\BackEndBundle\Entity\Client'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'facturacohete_backendbundle_client';
    }
}
