<?php

namespace FacturaCohete\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InstanceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName')
            ->add('companyType')
            ->add('registeredName')
            ->add('firstName')
            ->add('lastName')
            ->add('taxNumberName')
            ->add('taxNumber')
            ->add('address')
            ->add('phoneNumber')
            ->add('email')
            ->add('currency')
            ->add('taxes', 'collection', array(
                'type' => new TaxType(),
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('taxEnabled')
            ->add('discountEnabled')
            ->add('discountRate');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FacturaCohete\BackEndBundle\Entity\Instance'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'facturacohete_backendbundle_instance';
    }
}
