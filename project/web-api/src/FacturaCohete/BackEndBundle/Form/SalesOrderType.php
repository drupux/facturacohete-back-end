<?php

namespace FacturaCohete\BackEndBundle\Form;

use FacturaCohete\BackEndBundle\Form\DataTransformer\ClientToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        /** @var \FacturaCohete\BackEndBundle\Form\DataTransformer\ClientToIdTransformer $transformer */
        $transformer = new ClientToIdTransformer($entityManager);
        $builder
            ->add('taxEnabled')
            ->add('taxCode')
            ->add('taxRate')
            ->add('discountEnabled')
            ->add('discountRate')
            ->add('dateOfIssue')
            ->add('number')
            ->add($builder->create('client', 'text')->addModelTransformer($transformer))
            ->add('salesOrderLines', 'collection', array(
                'type' => new SalesOrderLineType(),
                'allow_add' => true,
                'allow_delete' => true,
                'options' => array(
                    'em' => $options['em'],
                )
            ))->add('payments', 'collection', array(
                'type' => new PaymentType(),
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('issuerRegisteredName')
            ->add('issuerCommercialName')
            ->add('issuerTaxNumber')
            ->add('issuerAddress')
            ->add('issuerPhoneNumber')
            ->add('issuerEmail')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->
        setDefaults(array(
                'data_class' => 'FacturaCohete\BackEndBundle\Entity\SalesOrder'
            ))
            ->setRequired(array(
                'em',
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\ORM\EntityManager',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'facturacohete_backendbundle_sales_order';
    }
}
