<?php

namespace FacturaCohete\BackEndBundle\Form;

use FacturaCohete\BackEndBundle\Form\DataTransformer\ItemToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalesOrderLineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        /** @var \FacturaCohete\BackEndBundle\Form\DataTransformer\ItemToIdTransformer $transformer */
        $transformer = new ItemToIdTransformer($entityManager);

        $builder
            ->add('price')
            ->add('quantity')
            ->add($builder->create('item', 'text')->addModelTransformer($transformer));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => 'FacturaCohete\BackEndBundle\Entity\SalesOrderLine'
            ))
            ->setRequired(array(
                'em',
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\ORM\EntityManager',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'facturacohete_backendbundle_sales_order_line';
    }


}
