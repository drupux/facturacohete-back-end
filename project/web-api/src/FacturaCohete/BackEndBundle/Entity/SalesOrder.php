<?php

namespace FacturaCohete\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\ExclusionPolicy;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Exclude;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\VirtualProperty;


/**
 * SalesOrder
 *
 * @ORM\Table(name="sales_order")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class SalesOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer")
     * @Groups({"details", "list"})
     */
    private $number;

    /**
     * @var \FacturaCohete\BackEndBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="FacturaCohete\BackEndBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     * @Groups({"details"})
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="client_name", type="string", length=255, nullable=true)
     * @Groups({"details", "list"})
     */
    private $clientName;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FacturaCohete\BackEndBundle\Entity\SalesOrderLine", mappedBy="salesOrder")
     * @Groups({"details"})
     */
    private $salesOrderLines;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FacturaCohete\BackEndBundle\Entity\Payment", mappedBy="salesOrder")
     * @Groups({"details"})
     */
    private $payments;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_enabled", type="boolean")
     * @Groups({"list", "details"})
     */
    private $taxEnabled;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_rate", type="float")
     * @Groups({"list", "details"})
     */
    private $taxRate;

    /**
     * @var float
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $taxCode;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_enabled", type="boolean")
     * @Groups({"list", "details"})
     */
    private $discountEnabled;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_rate", type="float")
     * @Groups({"list", "details"})
     */
    private $discountRate;

    /**
     * @var float
     *
     * @ORM\Column(name="subtotal", type="float")
     * @Groups({"list", "details"})
     */
    private $subtotal;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_to_date", type="float")
     * @Groups({"list", "details"})
     */
    private $paidToDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_issue", type="datetime")
     * @Groups({"list", "details"})
     */
    private $dateOfIssue;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_registered_name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerRegisteredName;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_commercial_name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerCommercialName;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_tax_number", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerTaxNumber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_address", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerAddress;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_phone_number", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerPhoneNumber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="issuer_email", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $issuerEmail;

    /**
     * @var \FacturaCohete\BackEndBundle\Entity\Instance
     *
     * @ORM\ManyToOne(targetEntity="FacturaCohete\BackEndBundle\Entity\Instance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="instance_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     * @Exclude
     */
    private $instance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups({"list", "details"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Groups({"list", "details"})
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Item
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set client
     *
     * @param Client $client
     * @return SalesOrder
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Get number
     *
     * @return integer
     * @VirtualProperty
     * @Groups({"list"})
     */
    public function getClientId()
    {
        if ($this->client) {
            return $this->client->getId();
        } else {
            return null;
        }
    }

    /**
     * Set clientName
     *
     * @param string $clientName
     * @return SalesOrder
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * Get clientName
     *
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set salesOrderLines
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $salesOrderLines
     * @return SalesOrder
     */
    public function setSalesOrderLines($salesOrderLines)
    {
        $this->salesOrderLines = $salesOrderLines;

        return $this;
    }

    /**
     * Get salesOrderLine
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSalesOrderLines()
    {
        return $this->salesOrderLines;
    }

    /**
     *
     * Remove sales order lines from sales order.
     *
     * @return SalesOrder
     */
    public function clearSalesOrderLines()
    {
        $this->salesOrderLines->clear();
        $this->setSubtotal(0);

        return $this;
    }

    /**
     * Set payments
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $payments
     * @return SalesOrder
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;

        return $this;
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     *
     * Remove payments from sales order.
     *
     * @return SalesOrder
     */
    public function clearPayments()
    {
        $this->payments->clear();
        $this->setPaidToDate(0);

        return $this;
    }

    /**
     * Set taxEnabled
     *
     * @param boolean $taxEnabled
     * @return SalesOrder
     */
    public function setTaxEnabled($taxEnabled)
    {
        $this->taxEnabled = $taxEnabled;

        return $this;
    }

    /**
     * Get taxEnabled
     *
     * @return boolean
     */
    public function getTaxEnabled()
    {
        return $this->taxEnabled;
    }

    /**
     * Set taxRate
     *
     * @param float $taxRate
     * @return SalesOrder
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * Get taxRate
     *
     * @return float
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * Set taxCode
     *
     * @param string $taxCode
     * @return SalesOrder
     */
    public function setTaxCode($taxCode)
    {
        $this->taxCode = $taxCode;

        return $this;
    }

    /**
     * Get taxCode
     *
     * @return string
     */
    public function getTaxCode()
    {
        return $this->taxRate;
    }

    /**
     * Set taxEnabled
     *
     * @param boolean $discountEnabled
     * @return SalesOrder
     */
    public function setDiscountEnabled($discountEnabled)
    {
        $this->discountEnabled = $discountEnabled;

        return $this;
    }

    /**
     * Get discountEnabled
     *
     * @return boolean
     */
    public function getDiscountEnabled()
    {
        return $this->discountEnabled;
    }

    /**
     * Get discountRate
     *
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * Set discountRate
     *
     * @param float $discountRate
     * @return SalesOrder
     */
    public function setDiscountRate($discountRate)
    {
        $this->discountRate = $discountRate;

        return $this;
    }

    /**
     * Set subtotal
     *
     * @param float $subtotal
     * @return SalesOrder
     */
    private function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal
     *
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set paidToDate
     *
     * @param float $paidToDate
     * @return SalesOrder
     */
    private function setPaidToDate($paidToDate)
    {
        $this->paidToDate = $paidToDate;

        return $this;
    }

    /**
     * Get paidToDate
     *
     * @return float
     */
    public function getPaidToDate()
    {
        return $this->paidToDate;
    }

    /**
     * Set dateOfIssue
     *
     * @param \DateTime $dateOfIssue
     * @return SalesOrder
     */
    public function setDateOfIssue($dateOfIssue)
    {
        $this->dateOfIssue = $dateOfIssue;

        return $this;
    }

    /**
     * Get dateOfIssue
     *
     * @return \DateTime
     */
    public function getDateOfIssue()
    {
        return $this->dateOfIssue;
    }

    /**
     * Set instance
     *
     * @param Instance $instance
     * @return SalesOrder
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * @return string
     */
    public function getIssuerRegisteredName()
    {
        return $this->issuerRegisteredName;
    }

    /**
     * @param string $issuerRegisteredName
     */
    public function setIssuerRegisteredName($issuerRegisteredName)
    {
        $this->issuerRegisteredName = $issuerRegisteredName;
    }

    /**
     * @return string
     */
    public function getIssuerCommercialName()
    {
        return $this->issuerCommercialName;
    }

    /**
     * @param string $issuerCommercialName
     */
    public function setIssuerCommercialName($issuerCommercialName)
    {
        $this->issuerCommercialName = $issuerCommercialName;
    }

    /**
     * @return string
     */
    public function getIssuerTaxNumber()
    {
        return $this->issuerTaxNumber;
    }

    /**
     * @param string $issuerTaxNumber
     */
    public function setIssuerTaxNumber($issuerTaxNumber)
    {
        $this->issuerTaxNumber = $issuerTaxNumber;
    }

    /**
     * @return string
     */
    public function getIssuerAddress()
    {
        return $this->issuerAddress;
    }

    /**
     * @param string $issuerAddress
     */
    public function setIssuerAddress($issuerAddress)
    {
        $this->issuerAddress = $issuerAddress;
    }

    /**
     * @return string
     */
    public function getIssuerEmail()
    {
        return $this->issuerEmail;
    }

    /**
     * @param string $issuerEmail
     */
    public function setIssuerEmail($issuerEmail)
    {
        $this->issuerEmail = $issuerEmail;
    }

    /**
     * @return string
     */
    public function getIssuerPhoneNumber()
    {
        return $this->issuerPhoneNumber;
    }

    /**
     * @param string $issuerPhoneNumber
     */
    public function setIssuerPhoneNumber($issuerPhoneNumber)
    {
        $this->issuerPhoneNumber = $issuerPhoneNumber;
    }

    /**
     * Get instance
     *
     * @return SalesOrder
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SalesOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return SalesOrder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->updateAggregatedData();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updateAggregatedData();
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Updates subtotal and amount paid
     */
    private function updateAggregatedData()
    {
        $subtotal = 0;
        /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrderLine $salesOrderLine */
        foreach ($this->getSalesOrderLines() as $salesOrderLine) {
            $subtotal = $subtotal + $salesOrderLine->getPrice() * $salesOrderLine->getQuantity();
        }
        $this->setSubtotal($subtotal);

        $paidToDate = 0;

        /** @var \FacturaCohete\BackEndBundle\Entity\Payment $payment */
        foreach ($this->getPayments() as $payment) {
            $paidToDate = $paidToDate + $payment->getAmount();
        }
        $this->setPaidToDate($paidToDate);

        if ($this->getClient()) {
            $this->setClientName($this->getClient()->getOrganizationName());
        }
    }
}
