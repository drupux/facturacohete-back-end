<?php

namespace FacturaCohete\BackEndBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\ExclusionPolicy;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Exclude;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Client
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_name", type="string", length=255)
     * @Groups({"list", "details"})
     */
    private $organizationName;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="province", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="business_phone", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $businessPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="internal_notes", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $internalNotes;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FacturaCohete\BackEndBundle\Entity\ClientContact", mappedBy="client")
     * @Groups({"details"})
     */
    private $clientContacts;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_contacts_quantity", type="integer")
     */
    private $clientContactsQuantity;

    /**
     * @var \FacturaCohete\BackEndBundle\Entity\Instance
     *
     * @ORM\ManyToOne(targetEntity="FacturaCohete\BackEndBundle\Entity\Instance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="instance_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     * @Exclude
     */
    private $instance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     *
     */
    public function __construct()
    {
        $this->clientContacts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     * @return Client
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Client
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Client
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set province
     *
     * @param string $province
     * @return Client
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Client
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set businessPhone
     *
     * @param string $businessPhone
     * @return Client
     */
    public function setBusinessPhone($businessPhone)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    /**
     * Get businessPhone
     *
     * @return string
     */
    public function getBusinessPhone()
    {
        return $this->businessPhone;
    }

    /**
     * Set internalNotes
     *
     * @param string $internalNotes
     * @return Client
     */
    public function setInternalNotes($internalNotes)
    {
        $this->internalNotes = $internalNotes;

        return $this;
    }

    /**
     * Get internalNotes
     *
     * @return string
     */
    public function getInternalNotes()
    {
        return $this->internalNotes;
    }

    /**
     * Set instance
     *
     * @param \FacturaCohete\BackEndBundle\Entity\Instance $instance
     * @return Client
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * Get instance
     *
     * @return \FacturaCohete\BackEndBundle\Entity\Instance
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param ArrayCollection $clientContacts
     * @return Client
     */
    public function setClientContacts($clientContacts)
    {
        $this->clientContacts = $clientContacts;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getClientContacts()
    {
        return $this->clientContacts;
    }

    /**
     * Set clientContactsQuantity
     *
     * @param integer $clientContactsQuantity
     * @return Item
     */
    public function setClientContactsQuantity($clientContactsQuantity)
    {
        $this->clientContactsQuantity = $clientContactsQuantity;

        return $this;
    }

    /**
     * Get clientContactsQuantity
     *
     * @return integer
     */
    public function getClientContactsQuantity()
    {
        return $this->clientContactsQuantity;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Client
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Client
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->updateAggregatedData();
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updateAggregatedData();
        $this->setUpdatedAt(new \DateTime());
    }


    /**
     * Updates organization name
     */
    private function updateAggregatedData()
    {
        if ($this->getOrganizationName() == "") {
            /** @var \FacturaCohete\BackEndBundle\Entity\ClientContact $firstClientContact */
            $firstClientContact = $this->getClientContacts()->first();
            if ($firstClientContact) {
                $this->setOrganizationName($firstClientContact->getName() . " " . $firstClientContact->getLastName());
            }
        }
        $this->setClientContactsQuantity($this->getClientContacts()->count());
    }

}
