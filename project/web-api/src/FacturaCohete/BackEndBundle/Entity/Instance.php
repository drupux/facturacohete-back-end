<?php

namespace FacturaCohete\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;

/**
 * Instance
 *
 * @ORM\Table(name="instance")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Instance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "details"})
     */
    private $id;

    /**
     * Nombre comercial o nombre de fantasía
     *
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255)
     * @Groups({"list", "details"})
     */
    private $companyName;

    /**
     * Tipo de contribuyente -> Físico nacional o Jurídico
     *
     * @var string
     *
     * @ORM\Column(name="company_type", type="string", length=255)
     * @Groups({"list", "details"})
     */
    private $companyType;

    /**
     * Razón social en caso que es compañia jurídica
     *
     * @var string
     *
     * @ORM\Column(name="registered_name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $registeredName;

    /**
     * Nombre en caso que es físico nacional
     *
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $firstName;

    /**
     * Apellido en caso que también de físico nacional
     *
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $lastName;

    /**
     * Cédula Fisica, cédula jurídica, etc.
     *
     * @var string
     *
     * @ORM\Column(name="tax_number_name", type="string", length=255)
     * @Groups({"list", "details"})
     */
    private $taxNumberName;

    /**
     * número de cédula físico o jurídico
     *
     * @var string
     *
     * @ORM\Column(name="tax_number", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $taxNumber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $address;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $phoneNumber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255, nullable=true)
     * @Groups({"list", "details"})
     */
    private $currency;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tax_enabled", type="boolean")
     * @Groups({"details", "list"})
     */
    private $taxEnabled;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FacturaCohete\BackEndBundle\Entity\Tax", mappedBy="instance")
     * @Groups({"list", "details"})
     */
    private $taxes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discount_enabled", type="boolean")
     * @Groups({"details", "list"})
     */
    private $discountEnabled;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_rate", type="float")
     * @Groups({"details", "list"})
     */
    private $discountRate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups({"list", "details"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Groups({"list", "details"})
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Instance
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyType
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;
    }

    /**
     * @return string
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * @param string $registeredName
     */
    public function setRegisteredName($registeredName)
    {
        $this->registeredName = $registeredName;
    }

    /**
     * @return string
     */
    public function getRegisteredName()
    {
        return $this->registeredName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $taxNumberName
     */
    public function setTaxNumberName($taxNumberName)
    {
        $this->taxNumberName = $taxNumberName;
    }

    /**
     * @return string
     */
    public function getTaxNumberName()
    {
        return $this->taxNumberName;
    }

    /**
     * @param string $taxNumber
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;
    }

    /**
     * @return string
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Instance
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param boolean $taxEnabled
     */
    public function setTaxEnabled($taxEnabled)
    {
        $this->taxEnabled = $taxEnabled;
    }

    /**
     * @return boolean
     */
    public function isTaxEnabled()
    {
        return $this->taxEnabled;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $taxes
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param boolean $discountEnabled
     */
    public function setDiscountEnabled($discountEnabled)
    {
        $this->discountEnabled = $discountEnabled;
    }

    /**
     * @return boolean
     */
    public function isDiscountEnabled()
    {
        return $this->discountEnabled;
    }

    /**
     * @param float $discountRate
     */
    public function setDiscountRate($discountRate)
    {
        $this->discountRate = $discountRate;
    }

    /**
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Instance
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Instance
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}
