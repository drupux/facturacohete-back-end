<?php

namespace FacturaCohete\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;

/**
 * SalesOrderLine
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class SalesOrderLine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"details", "list"})
     */
    private $id;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="FacturaCohete\BackEndBundle\Entity\Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     * @Groups({"details"})

     */
    private $item;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=255)
     * @Groups({"details", "list"})
     */
    private $itemName;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     * @Groups({"details", "list"})
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     * @Groups({"details", "list"})
     */
    private $quantity;

    /**
     * @var \FacturaCohete\BackEndBundle\Entity\SalesOrder
     *
     * @ORM\ManyToOne(targetEntity="SalesOrder", inversedBy="salesOrderLines")
     * @ORM\JoinColumn(name="sales_order_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     **/
    private $salesOrder;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param Item $item
     * @return SalesOrderLine
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     * @return SalesOrderLine
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return SalesOrderLine
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return SalesOrderLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set salesOrder
     *
     * @param SalesOrder $salesOrder
     * @return SalesOrderLine
     */
    public function setSalesOrder($salesOrder)
    {
        $this->salesOrder = $salesOrder;

        return $this;
    }

    /**
     * Get salesOrder
     *
     * @return SalesOrder
     */
    public function getSalesOrder()
    {
        return $this->quantity;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setItemName($this->getItem()->getName());

    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setItemName($this->getItem()->getName());

    }
}
