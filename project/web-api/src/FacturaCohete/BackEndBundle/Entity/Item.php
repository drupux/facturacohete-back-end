<?php

namespace FacturaCohete\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\ExclusionPolicy;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Exclude;
use /** @noinspection PhpUnusedAliasInspection */
    JMS\Serializer\Annotation\Groups;
/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ExclusionPolicy("NONE")
 */
class Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"details", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"details", "list"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Groups({"details", "list"})
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     * @Groups({"details", "list"})
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="track_inventory", type="boolean")
     * @Groups({"details", "list"})
     */
    private $trackInventory;

    /**
     * @var integer
     *
     * @ORM\Column(name="inventory", type="integer")
     * @Groups({"details", "list"})
     */
    private $inventory;

    /**
     * @var \FacturaCohete\BackEndBundle\Entity\Instance
     *
     * @ORM\ManyToOne(targetEntity="FacturaCohete\BackEndBundle\Entity\Instance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="instance_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     * @Exclude
     */
    private $instance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set trackInventory
     *
     * @param boolean $trackInventory
     * @return Item
     */
    public function setTrackInventory($trackInventory)
    {
        $this->trackInventory = $trackInventory;

        return $this;
    }

    /**
     * Get trackInventory
     *
     * @return boolean
     */
    public function getTrackInventory()
    {
        return $this->trackInventory;
    }

    /**
     * Set inventory
     *
     * @param integer $inventory
     * @return Item
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory
     *
     * @return integer
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set instance
     *
     * @param \FacturaCohete\BackEndBundle\Entity\Instance $instance
     * @return Item
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * @return \FacturaCohete\BackEndBundle\Entity\Instance
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Item
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}
