<?php

namespace FacturaCohete\BackEndBundle\Controller;


use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserController extends AppController
{
    /**
     * @return \FacturaCohete\BackEndBundle\Handler\UserHandler
     *
     */
    private function getUserHandler()
    {
        return $this->container->get('factura_cohete.user_handler');
    }

    /**
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function loginCheckAction(Request $request)
    {
        $user = $this->getUserHandler()->getByUsernameAndPassword($this->get('request')->request->get('username'), $this->get('request')->request->get('password'));
        if (!$user) {
            return $this->renderNotFound($this->get('request')->request->get('username'));
        } else {
            return $user;
        }
    }

    /**
     * List specific user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @param int $id the user id
     * @return array
     */
    public function getUserAction($id)
    {
        $item = $this->getUserHandler()->get($this->getUser(), $id);
        if (!$item) {
            return $this->renderNotFound($id);
        } else {
            return $item;
        }
    }


    /**
     * List all users.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="per_page", requirements="\d+", default="10", description="How many pages to return.")
     *
     * @Annotations\View(
     *  templateVar="users"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getUsersAction(ParamFetcherInterface $paramFetcher)
    {
        $per_page = $paramFetcher->get('per_page');
        $page = $paramFetcher->get('page');
        return $this->getUserHandler()->getByPage($this->getUser(), $page, $per_page);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   input = "FacturaCohete\BackEndBundle\Form\UserType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postUserAction(Request $request)
    {
        try {

            $newUser = $this->getUserHandler()->post(
                $this->getUser(),
                $request->request->all()
            );
            $routeOptions = array(
                'id' => $newUser->getId(),
            );
            return $this->routeRedirectView('api_get_user', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing user from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "FacturaCohete\BackEndBundle\Form\UserType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function putUserAction(Request $request, $id)
    {
        if (!($user = $this->getUserHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $user = $this->getUserHandler()->put(
                $this->getUser(),
                $user,
                $request->request->all()
            );
            $routeOptions = array(
                'id' => $user->getId(),
            );
            return $this->routeRedirectView('api_get_user', $routeOptions, Codes::HTTP_NO_CONTENT);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete existing user from the submitted id.",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param int $id the user id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteUserAction($id)
    {
        if (!($item = $this->getUserHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $this->getUserHandler()->delete($item);
            return $this->renderNoContent("Deleted successfully");
        }
    }

    /**
     * Register new user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function registerAction(Request $request)
    {
        try {
            /** @var \FacturaCohete\BackEndBundle\Entity\User $newUser */
            $newUser = $this->getUserHandler()->register($request->request->all());
            if ($newUser->getEmailVerified()) {
                return $this->renderConflict("Email already associated with an account.");
            } else {
                return $newUser->getToken();
            }
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Get verification token
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function requestVerificationTokenAction(Request $request)
    {
        $params = $request->request->all();
        if (!array_key_exists('email', $params)) {
            return $this->renderBadRequest("[email] parameters missing.");
        }
        $email = $params["email"];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->getUserHandler()->getByEmail($email);
        //TODO: Check if token has expired
        if (!$user) {
            return $this->renderNotFound($email);
        } else {
            if ($user->getEmailVerified()) {
                return $this->renderNotAcceptable("Email already verified. No verification token available.");

            } else {
                return $user->getToken();
            }
        }
    }


    /**
     * Register new user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function requestPasswordResetTokenAction(Request $request)
    {
        $params = $request->request->all();
        if (!array_key_exists('email', $params)) {
            return $this->renderBadRequest("[email] parameters missing.");
        }
        $email = $params["email"];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->getUserHandler()->getByEmail($email);
        //TODO: Check if token has expired
        if (!$user) {
            return $this->renderNotFound($email);
        } else {
            if ($user->getEmailVerified()) {
                $user = $this->getUserHandler()->requestPasswordResetToken($user);

                return $user->getToken();
            } else {
                return $this->renderNotAcceptable("Email has not been verified.");
            }
        }
    }


    /**
     * Resets password
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @param Request $request
     * @return array
     */
    public function resetPasswordAction(Request $request)
    {
        $params = $request->request->all();
        if (!array_key_exists('token', $params) || !array_key_exists('password', $params)) {
            return $this->renderBadRequest("[token] or [password] parameters missing.");
        }

        $token = $params["token"];
        $password = $params["password"];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->getUserHandler()->verifyToken($token);
        if (!$user) {
            return $this->renderNotFound($token);
        } else {
            $this->getUserHandler()->changePassword($user, $password);
            return $user;
        }
    }


    /**
     * Verifies email address.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @param Request $request
     * @return array
     */
    public function verifyEmailAction(Request $request)
    {
        $params = $request->request->all();
        if (!array_key_exists('token', $params)) {
            return $this->renderBadRequest();
        }

        $token = $request->request->all()["token"];
        $user = $this->getUserHandler()->verifyToken($token);
        if (!$user) {
            return $this->renderNotFound($token);
        } else {
            return $user;
        }
    }


    /**
     * Updates password
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function updatePasswordAction(Request $request, $id)
    {
        $params = $request->request->all();
        if (!array_key_exists('password', $params)) {
            return $this->renderBadRequest("[password] parameters missing.");
        }
        $password = $params["password"];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->getUserHandler()->get($this->getUser(), $id);

        if (!$user) {
            return $this->renderNotFound($id);
        } else {
            $this->getUserHandler()->changePassword($user, $password);
            return $user;
        }
    }


    /**
     * Updates email
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="user"
     * )
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function updateEmailAction(Request $request, $id)
    {
        $params = $request->request->all();
        if (!array_key_exists('email', $params)) {
            return $this->renderBadRequest("[email] parameters missing.");
        }
        $email = $params["email"];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->getUserHandler()->get($this->getUser(), $id);

        if (!$user) {
            return $this->renderNotFound($id);
        } else {
            $this->getUserHandler()->changeEmail($user, $email);
            return $user;
        }
    }

}
