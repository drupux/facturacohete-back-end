<?php

namespace FacturaCohete\BackEndBundle\Controller;


use FacturaCohete\BackEndBundle\Entity\Client;
use FacturaCohete\BackEndBundle\Entity\ClientContact;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;


class InstanceController extends AppController
{

    /**
     * @return \FacturaCohete\BackEndBundle\Handler\InstanceHandler
     *
     */
    private function getHandler()
    {
        return $this->container->get('factura_cohete.instance_handler');
    }

    /**
     * List specific instance
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="instance"
     * )
     *
     * @param int $id the sales order id
     * @View(serializerGroups={"details"})
     * @return array
     */
    public function getInstanceAction($id)
    {
        $instance = $this->getHandler()->getById($id);
        if (!$instance) {
            return $this->renderNotFound($id);
        } else {
            return $instance;
        }
    }


    /**
     * Update existing instance from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "FacturaCohete\BackEndBundle\Form\InstanceType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function putInstanceAction(Request $request, $id)
    {

        /** @var \FacturaCohete\BackEndBundle\Entity\Instance $instance */
        if (!($instance = $this->getHandler()->getById($id))) {
            return $this->renderNotFound($id);
        } else {

            try {

                $salesOrder = $this->getHandler()->put(
                    $instance,
                    $request->request->all()
                );

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }

            $routeOptions = array(
                'id' => $salesOrder->getId(),
            );
            return $this->routeRedirectView('api_get_instance', $routeOptions, Codes::HTTP_NO_CONTENT);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete existing client from the submitted id.",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param int $id the item id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteInstanceAction($id)
    {
        if (!($salesOrder = $this->getHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $this->getHandler()->delete($salesOrder);
            return $this->renderNoContent("Deleted successfully");
        }
    }
}
