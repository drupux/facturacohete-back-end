<?php

namespace FacturaCohete\BackEndBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Util\Codes;


class AppController extends FOSRestController
{
    public function renderNotFound($id)
    {
        return $this->view(sprintf('The resource \'%s\' was not found.', $id), Codes::HTTP_NOT_FOUND);
    }

    public function renderNoContent($message = "Success")
    {
        return $this->view($message, Codes::HTTP_NO_CONTENT);
    }

    public function renderBadRequest($message = "The request could not be understood by the server due to malformed syntax.")
    {
        return $this->view($message, Codes::HTTP_BAD_REQUEST);
    }

    public function renderNotAcceptable($message = "Preconditions not met to response to this request")
    {
        return $this->view($message, Codes::HTTP_NOT_ACCEPTABLE);
    }
    public function renderConflict($message = "The request could not be completed due to a conflict with the current state of the resource.")
    {
        return $this->view($message, Codes::HTTP_CONFLICT);
    }




}
