<?php

namespace FacturaCohete\BackEndBundle\Controller;


use FacturaCohete\BackEndBundle\Entity\Client;
use FacturaCohete\BackEndBundle\Entity\ClientContact;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;


class ClientController extends AppController
{

    /**
     * @return \FacturaCohete\BackEndBundle\Handler\ClientHandler
     *
     */
    private function getClientHandler()
    {
        return $this->container->get('factura_cohete.client_handler');
    }

    /**
     * List specific item
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="clients"
     * )
     *
     * @param int $id the item id
     * @return array
     */
    public function getClientAction($id)
    {
        $item = $this->getClientHandler()->get($this->getUser(), $id);
        if (!$item) {
            return $this->renderNotFound($id);
        } else {
            return $item;
        }
    }

    /**
     * List all items.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="per_page", requirements="\d+", default="10", description="How many pages to return.")
     *
     * @Annotations\View(
     *  templateVar="items"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getClientsAction(ParamFetcherInterface $paramFetcher)
    {
        $per_page = $paramFetcher->get('per_page');
        $page = $paramFetcher->get('page');
        return $this->getClientHandler()->getByPage($this->getUser(), $page, $per_page);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new client from the submitted data.",
     *   input = "FacturaCohete\BackEndBundle\Form\ClientType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postClientAction(Request $request)
    {

        try {
            /** @var \FacturaCohete\BackEndBundle\Entity\Client $newClient */
            $newClient = $this->getClientHandler()->post(
                $this->getUser(),
                $request->request->all()
            );
            $routeOptions = array(
                'id' => $newClient->getId(),
            );
            return $this->routeRedirectView('api_get_client', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }


    /**
     * Update existing client from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "FacturaCohete\BackEndBundle\Form\ClientType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function putClientAction(Request $request, $id)
    {
        if (!($item = $this->getClientHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            try {
                $item = $this->getClientHandler()->put(
                    $this->getUser(),
                    $item,
                    $request->request->all()
                );
            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
            $routeOptions = array(
                'id' => $item->getId(),
            );
            return $this->routeRedirectView('api_get_client', $routeOptions, Codes::HTTP_NO_CONTENT);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete existing client from the submitted id.",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param int $id the item id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteClientAction($id)
    {
        if (!($item = $this->getClientHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $this->getClientHandler()->delete($item);
            return $this->renderNoContent("Deleted successfully");
        }

    }
}
