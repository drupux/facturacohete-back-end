<?php

namespace FacturaCohete\BackEndBundle\Controller;


use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;


class ItemController extends AppController
{

    /**
     * @return \FacturaCohete\BackEndBundle\Handler\ItemHandler
     *
     */
    private function getItemHandler()
    {
        return $this->container->get('factura_cohete.item_handler');
    }

    /**
     * List specific item
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="items"
     * )
     *
     * @param int $id the item id
     * @return array
     */
    public function getItemAction($id)
    {
        $item = $this->getItemHandler()->get($this->getUser(), $id);
        if (!$item) {
            return $this->renderNotFound($id);
        } else {
            return $item;
        }
    }

    /**
     * List all items.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="per_page", requirements="\d+", default="10", description="How many pages to return.")
     *
     * @Annotations\View(
     *  templateVar="items"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getItemsAction(ParamFetcherInterface $paramFetcher)
    {
        $per_page = $paramFetcher->get('per_page');
        $page = $paramFetcher->get('page');
        return $this->getItemHandler()->getByPage($this->getUser(), $page, $per_page);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new item from the submitted data.",
     *   input = "FacturaCohete\BackEndBundle\Form\ItemType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postItemAction(Request $request)
    {
        try {

            $newItem = $this->getItemHandler()->post(
                $this->getUser(),
                $request->request->all()
            );
            $routeOptions = array(
                'id' => $newItem->getId(),
            );
            return $this->routeRedirectView('api_get_item', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing page from the submitted data or create a new page at a specific location.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "FacturaCohete\BackEndBundle\Form\ItemType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function putItemAction(Request $request, $id)
    {
        if (!($item = $this->getItemHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            try {
                $item = $this->getItemHandler()->put(
                    $this->getUser(),
                    $item,
                    $request->request->all()
                );
            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
            $routeOptions = array(
                'id' => $item->getId(),
            );
            return $this->routeRedirectView('api_get_item', $routeOptions, Codes::HTTP_NO_CONTENT);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete existing item from the submitted id.",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param int $id the item id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteItemAction($id)
    {
        if (!($item = $this->getItemHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $this->getItemHandler()->delete($item);
            return $this->renderNoContent("Deleted successfully");
        }
    }


}
