<?php

namespace FacturaCohete\BackEndBundle\Controller;

use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;


class SalesOrderController extends AppController
{
    /**
     * @return \FacturaCohete\BackEndBundle\Handler\SalesOrderHandler
     *
     */
    private function getHandler()
    {
        return $this->container->get('factura_cohete.sales_order_handler');
    }

    /**
     * List specific Sales Order
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar="sales order"
     * )
     *
     * @param int $id the sales order id
     * @View(serializerGroups={"details"})
     * @return array
     */
    public function getSales_orderAction($id)
    {
        $item = $this->getHandler()->get($this->getUser(), $id);
        if (!$item) {
            return $this->renderNotFound($id);
        } else {
            return $item;
        }
    }

    /**
     * List all sales orders.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="page", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="per_page", requirements="\d+", default="10", description="How many pages to return.")
     * @Annotations\QueryParam(name="sort", default="id", description="Property to sort by")

     *
     * @Annotations\View(
     *  templateVar="sales orders"
     * )
     *
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @View(serializerGroups={"list"})
     * @return array
     */
    public function getSales_ordersAction(ParamFetcherInterface $paramFetcher)
    {
        $per_page = $paramFetcher->get('per_page');
        $page = $paramFetcher->get('page');
        $sort = $paramFetcher->get('sort');
        return $this->getHandler()->getByPage($this->getUser(), $page, $per_page, $sort);
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new sales order from the submitted data.",
     *   input = "FacturaCohete\BackEndBundle\Form\SalesOrderType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     *
     * @Rest\View
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postSales_orderAction(Request $request)
    {
        try {
            /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrder $newSalesOrder */
            $newSalesOrder = $this->getHandler()->post(
                $this->getUser(),
                $request->request->all()
            );
            $routeOptions = array(
                'id' => $newSalesOrder->getId(),
            );
            return $this->routeRedirectView('api_get_sales_order', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing sales order from the submitted data or create a new page at a specific location.
     *
     * @ApiDoc(
     *   resource = true,
     *   input = "FacturaCohete\BackEndBundle\Form\SalesOrderType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     *
     * @Rest\View
     * @param Request $request
     * @param $id
     * @return \FOS\RestBundle\View\View
     */
    public function putSales_orderAction(Request $request, $id)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrder $salesOrder */
        if (!($salesOrder = $this->getHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            try {
                $salesOrder = $this->getHandler()->put(
                    $this->getUser(),
                    $salesOrder,
                    $request->request->all()
                );
            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
            $routeOptions = array(
                'id' => $salesOrder->getId(),
            );
            return $this->routeRedirectView('api_get_item', $routeOptions, Codes::HTTP_NO_CONTENT);
        }
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete existing client from the submitted id.",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     404 = "Returned when not found",
     *   }
     * )
     * @Rest\View
     * @param int $id the item id
     * @return \FOS\RestBundle\View\View
     */
    public function deleteSales_orderAction($id)
    {

        if (!($salesOrder = $this->getHandler()->get($this->getUser(), $id))) {
            return $this->renderNotFound($id);
        } else {
            $this->getHandler()->delete($salesOrder);
            return $this->renderNoContent("Deleted successfully");
        }
    }
}
