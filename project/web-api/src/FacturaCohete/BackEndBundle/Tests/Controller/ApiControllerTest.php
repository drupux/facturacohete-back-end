<?php

namespace FacturaCohete\BackEndBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    private $client;

    public function setUp()
    {
    }

    protected function getIdFromLocation($location)
    {
        $startIndex = strrpos($location, '/') + 1;
        $logger = $this->client->getContainer()->get('logger');
        $logger->info("headers->" . substr($location, $startIndex));
        return substr($location, $startIndex);
    }

    public function testApiMethods()
    {
        $open_client = static::createClient();
        $user = date_timestamp_get(new \DateTime()) . '@domain.com';
        $password = "pass";
        $data = '{"organization_name":"aa","email":"' . $user . '","password":"' . $password . '"}';
        $open_client->request('POST', '/register',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());

        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => $user,
            'PHP_AUTH_PW' => $password,
        ));

        $resources = array
        (
            array(
                "name" => "items",
                "post_data" => '{"name":"test item","description":"Long descriptions about the item","price":232.2,"track_inventory":true,"inventory":43}',
                "post_invalid_data" => '{"nameXXX":"test item","description":"Long descriptions about the item","price":232.2,"track_inventory":true,"inventory":43}'
            ),
            array(
                "name" => "clients",
                "post_data" => '{"organization_name":"","country":"cr","street":"calle","city":"alaj","province":"ala","postal_code":"505","business_phone":"3423","internal_notes":"notas","client_contacts":[{"name":"aaaa","last_name":"apellido"},{"name":"n","last_name":"apellido2","email":"e","phone1":"rqerqr","phone2":"ere"}]}',
                "post_invalid_data" => '{"organization_nameXXX":"","country":"cr","street":"calle","city":"alaj","province":"ala","postal_code":"505","business_phone":"3423","internal_notes":"notas","client_contacts":[{"name":"aaaa","last_name":"apellido"},{"name":"n","last_name":"apellido2","email":"e","phone1":"rqerqr","phone2":"ere"}]}'
            ),
            array(
                "name" => "users",
                "post_data" => '{"email":"' . date_timestamp_get(new \DateTime()) . '@company.com","password":"hola"}',
                "post_invalid_data" => '{"emailXXX":"' . date_timestamp_get(new \DateTime()) . '@company.com","password":"hola"}'
            )
        );
        foreach ($resources as $resource) {
            $this->client->request('GET', '/' . $resource["name"] . '/9999');
            $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

            $this->client->request('GET', '/' . $resource["name"]);
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

            if (array_key_exists('post_invalid_data', $resource)) {
                $this->client->request('POST', '/' . $resource["name"],
                    array(),
                    array(),
                    array('CONTENT_TYPE' => 'application/json'),
                    $resource["post_invalid_data"]
                );
                $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
            }

            $this->client->request('POST', '/' . $resource["name"],
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                $resource["post_data"]//correct data sent.
            );
            $response = $this->client->getResponse();
            $this->assertEquals(201, $response->getStatusCode());

            $id = $this->getIdFromLocation($response->headers->get("Location"));

            $this->client->request('PUT', '/' . $resource["name"] . '/99999',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                $resource["post_data"]
            );
            $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

            $this->client->request('PUT', '/' . $resource["name"] . '/' . $id,
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json'),
                $resource["post_data"]
            );
            $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

            $this->client->request('GET', '/' . $resource["name"] . '/' . $id);
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

            $this->client->request('DELETE', '/' . $resource["name"] . '/99999');
            $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

            $this->client->request('DELETE', '/' . $resource["name"] . '/' . $id);
            $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
        }
    }

    public function testSalesOrders()
    {
        $open_client = static::createClient();


        $user = date_timestamp_get(new \DateTime()) . '@domain.com';
        $password = "pass";
        $data = '{"email":"' . $user . '","password":"' . $password . '"}';
        $open_client->request('POST', '/register',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );

        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());


        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => $user,
            'PHP_AUTH_PW' => $password,
        ));


        $items_data = '{"name":"test item","description":"Long descriptions about the item","price":232.2,"track_inventory":true,"inventory":43}';
        $clients_data = '{"organization_name":"","country":"cr","street":"calle","city":"alaj","province":"ala","postal_code":"505","business_phone":"3423","internal_notes":"notas","client_contacts":[{"name":"aaaa","last_name":"apellido"},{"name":"n","last_name":"apellido2","email":"e","phone1":"rqerqr","phone2":"ere"}]}';

        $this->client->request('POST', '/items',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $items_data
        );
        $response = $this->client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $itemId = $this->getIdFromLocation($response->headers->get("Location"));


        $this->client->request('POST', '/clients',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $clients_data
        );
        $response = $this->client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $clientId = $this->getIdFromLocation($response->headers->get("Location"));

        $sales_orders_data = '{"client":' . $clientId . ',"number":2 ,"tax_enabled":false,"tax_rate":10, "discount_enabled":true,"discount_rate":3, "date_of_issue":{"date":{"year":2011,"month":3,"day":3},"time":{"hour":3,"minute":3}}, "sales_order_lines":[{"item":' . $itemId . ',"quantity":2,"price":2343}],"payments":[{"amount":34,"method":"CASH","notes":"notas largas"}]}';
        $this->client->request('POST', '/sales_orders',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $sales_orders_data
        );
        $response = $this->client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $sales_order_id = $this->getIdFromLocation($response->headers->get("Location"));

        $sales_orders_invalid_data = '{"clientXXX":' . $clientId . ',"tax_enabled":false,"tax_rate":10,"discount_enabled":true,"discount_rate":3, "date_of_issue":{"date":{"year":2011,"month":3,"day":3},"time":{"hour":3,"minute":3}}, "sales_order_lines":[{"item":' . $itemId . ',"quantity":2,"price":2343}],"payments":[{"amount":34,"method":"CASH","notes":"notas largas"}]}';
        $this->client->request('POST', '/sales_orders',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $sales_orders_invalid_data
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/sales_orders/' . '9999');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/sales_orders/' . $sales_order_id);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/sales_orders');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/sales_orders/99999',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $sales_orders_data
        );
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/sales_orders/' . $sales_order_id,
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $sales_orders_data
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->client->request('DELETE', '/sales_orders/99999');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $this->client->request('DELETE', '/sales_orders/' . $sales_order_id);
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }
}
