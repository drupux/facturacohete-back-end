<?php

namespace FacturaCohete\BackEndBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

    private $client;

    public function setUp()
    {

    }


    public function testRegistration()
    {


        $open_client = static::createClient();
        $user = date_timestamp_get(new \DateTime()) . '@domain.com';
        $password = "pass";
        $data = '{"organization_name":"registration name","email":"' . $user . '","password":"' . $password . '"}';
        $open_client->request('POST', '/register',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );


        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());//register new user
        $response = $open_client->getResponse();

        $logger = $open_client->getContainer()->get('logger');
        $token = $response->getContent();
        $logger->info("Registration Token->[" . $response->getContent() . "]");

        //--------------------------------------------------------------------------------



        $this->client = static::createClient();

        $data = '{"username":"' . $user . '","password":"' . $password . 'XXXX"}';
        $open_client->request('POST', '/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(404, $open_client->getResponse()->getStatusCode());//login into the app with wrong credentials

        //--------------------------------------------------------------------------------


        $data = '{"username":"' . $user . '","password":"' . $password . '"}';
        $open_client->request('POST', '/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );

        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());//login into the app

        $logger = $open_client->getContainer()->get('logger');
        $logger->info("userJson" . json_decode($open_client->getResponse()->getContent(), true)['email_verified']);
        $logger->info("otro_token" . $token);
        $this->assertFalse(json_decode($open_client->getResponse()->getContent(), true)['email_verified']);//assert email has not being verified

        //--------------------------------------------------------------------------------

        $open_client = static::createClient();

        $data = '{"email":"' . $user . '" }';
        $open_client->request('POST', '/request_password_reset_token',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $response = $open_client->getResponse();
        $this->assertEquals(406, $response->getStatusCode());//Request password reset token with unverified email

        //--------------------------------------------------------------------------------

        $open_client = static::createClient();

        $data = '{"emailee":"' . $user . '" }';
        $open_client->request('POST', '/request_password_reset_token',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $response = $open_client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());//Request password reset token with wrong params

        //--------------------------------------------------------------------------------
        $open_client = static::createClient();

        $data = '{"email":"non existing email" }';
        $open_client->request('POST', '/request_password_reset_token',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $response = $open_client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());//Request password reset token with wrong email

        //--------------------------------------------------------------------------------


        $data = '{"token":' . $token . ' }';
        $open_client->request('POST', '/verify',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());//Verify email
        $this->assertTrue(json_decode($open_client->getResponse()->getContent(), true)['email_verified']);

        //--------------------------------------------------------------------------------

        $data = '{"organization_name":"registration name","email":"' . $user . '","password":"' . $password . '"}';
        $open_client->request('POST', '/register',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(409, $open_client->getResponse()->getStatusCode());//repeat email

        //--------------------------------------------------------------------------------

        $open_client = static::createClient();

        $data = '{"email":"' . $user . '" }';
        $open_client->request('POST', '/request_password_reset_token',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $response = $open_client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());//Request password reset token with verified valid email
        $passwordResetToken = $response->getContent();
        $logger->info("User Token->[" . $response->getContent() . "]");

        //--------------------------------------------------------------------------------

        $open_client = static::createClient();
        $data = '{"token":"WRONG_TOKEN" }';
        $open_client->request('POST', '/verify',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(404, $open_client->getResponse()->getStatusCode());//verify email with wrong email

        //--------------------------------------------------------------------------------
        $open_client = static::createClient();

        $data = '{"tokenTTT":"TOKEN" }';
        $open_client->request('POST', '/verify',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(400, $open_client->getResponse()->getStatusCode());//verify email with wrong params

        //--------------------------------------------------------------------------------

        $newPassword = ' new password';
        $data = '{"token":' . $passwordResetToken . ',"password":"' . $newPassword . '"}';
        $open_client->request('PATCH', '/reset_password',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());//Reset password with valid token

        //--------------------------------------------------------------------------------

        $newPassword = ' new password';
        $data = '{"token":"no valid token","password":"' . $newPassword . '"}';
        $open_client->request('PATCH', '/reset_password',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(404, $open_client->getResponse()->getStatusCode());//Reset password with invalid token

        //--------------------------------------------------------------------------------
        $newPassword = ' new password';
        $data = '{"token":"no valid token"}';
        $open_client->request('PATCH', '/reset_password',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );
        $this->assertEquals(400, $open_client->getResponse()->getStatusCode());//Reset password with invalid params

        //--------------------------------------------------------------------------------

        $data = '{"username":"' . $user . '","password":"' . $newPassword . '"}';
        $open_client->request('POST', '/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $data
        );

        $this->assertEquals(200, $open_client->getResponse()->getStatusCode());//login with new password

        //--------------------------------------------------------------------------------
    }

}
