<?php

namespace FacturaCohete\BackEndBundle\Handler;

use Symfony\Component\Form\Exception;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;


class EntityHandler
{
    /**
     * @param \Doctrine\ORM\EntityManager $om
     * @param $entityClass
     * @param \Symfony\Component\Form\FormFactory $formFactory
     */
    public function __construct($om, $entityClass, $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $id
     * @return mixed
     */
    public function get($user, $id)
    {
        return $this->repository->findOneBy(array('id' => $id, 'instance' => $user->getInstance()->getId()));
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $page
     * @param $limit
     * @param string $sort
     * @return array
     */
    public function getByPage($user, $page, $limit, $sort = 'id')
    {
        if ($sort[0] == "-") {
            $sort = substr($sort, 1);//removes '-' character
            $order = 'ASC';
        } else {
            $order = 'DESC';
        }

        $offset = (null == $page ? 0 : $page - 1) * $limit;
        return $this->repository->findBy(array('instance' => $user->getInstance()), array($sort => $order), $limit, $offset);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->om->remove($entity);
        $this->om->flush();
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     * @return mixed
     */
    protected function processForm($form)
    {
        if ($form->isValid()) {
            $entity = $form->getData();
            $this->om->persist($entity);
            $this->om->flush($entity);
            return $entity;
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }
}