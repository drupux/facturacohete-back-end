<?php

namespace FacturaCohete\BackEndBundle\Handler;

use FacturaCohete\BackEndBundle\Entity\SalesOrder;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use FacturaCohete\BackEndBundle\Form\SalesOrderType;

use Symfony\Component\Form\Exception;

class SalesOrderHandler extends EntityHandler
{
    public function __construct($em, $entityClass, $formFactory)
    {
        parent::__construct($em, $entityClass, $formFactory);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param array $parameters
     * @return mixed
     */
    public function post($user, array $parameters)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrder $newSalesOrder */
        $newSalesOrder = new SalesOrder();
        $newSalesOrder->setInstance($user->getInstance());
        $form = $this->formFactory->create(new SalesOrderType(), $newSalesOrder, array('method' => 'POST', 'em' => $this->om));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param \FacturaCohete\BackEndBundle\Entity\SalesOrder $salesOrder
     * @param array $parameters
     * @return mixed
     */
    public function put($user, $salesOrder, array $parameters)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrderLine $salesOrderLine */
        foreach ($salesOrder->getSalesOrderLines() as $salesOrderLine) {
            $this->om->remove($salesOrderLine);
        }
        $salesOrder->clearSalesOrderLines();

        /** @var \FacturaCohete\BackEndBundle\Entity\Payment $payment */
        foreach ($salesOrder->getPayments() as $payment) {
            $this->om->remove($payment);
        }
        $salesOrder->clearPayments();


        $salesOrder->setInstance($user->getInstance());
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(new SalesOrderType(), $salesOrder, array('method' => 'PUT', 'em' => $this->om));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * {@inheritdoc}
     */
    protected function processForm($form)
    {
        if ($form->isValid()) {
            /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrder $salesOrder */
            $salesOrder = $form->getData();
            /** @var \FacturaCohete\BackEndBundle\Entity\SalesOrderLine $salesOrderLine */
            foreach ($salesOrder->getSalesOrderLines() as $salesOrderLine) {
                $salesOrderLine->setSalesOrder($salesOrder);
                $this->om->persist($salesOrderLine);
            }

            foreach ($salesOrder->getPayments() as $payment) {
                $payment->setSalesOrder($salesOrder);
                $this->om->persist($payment);
            }

            $this->om->persist($salesOrder);
            $this->om->flush($salesOrder);
            return $salesOrder;
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }

}