<?php

namespace FacturaCohete\BackEndBundle\Handler;

use FacturaCohete\BackEndBundle\Form\InstanceType;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use Symfony\Component\Form\Exception;

class InstanceHandler extends EntityHandler
{
    public function __construct($em, $entityClass, $formFactory)
    {
        parent::__construct($em, $entityClass, $formFactory);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->repository->findOneBy(array('id' => $id));
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\Instance $instance
     * @param array $parameters
     * @return mixed
     */
    public function put($instance, array $parameters)
    {

        /** @var \FacturaCohete\BackEndBundle\Entity\Tax $tax */
        foreach ($instance->getTaxes() as $tax) {
            $this->om->remove($tax);
        }
        $instance->getTaxes()->clear();

        $form = $this->formFactory->create(new InstanceType(), $instance, array('method' => 'PUT'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * {@inheritdoc}
     */
    protected function processForm($form)
    {
        if ($form->isValid()) {
            /** @var \FacturaCohete\BackEndBundle\Entity\Instance $instance */
            $instance = $form->getData();
            /** @var \FacturaCohete\BackEndBundle\Entity\Tax $tax */
            foreach ($instance->getTaxes() as $tax) {
                $tax->setInstance($instance);
                $this->om->persist($tax);
            }
            $this->om->persist($instance);
            $this->om->flush($instance);
            return $instance;
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }

}