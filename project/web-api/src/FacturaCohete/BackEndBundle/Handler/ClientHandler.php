<?php

namespace FacturaCohete\BackEndBundle\Handler;

use FacturaCohete\BackEndBundle\Entity\Client;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;
use FacturaCohete\BackEndBundle\Form\ClientType;
use Symfony\Component\Form\Exception;

class ClientHandler extends EntityHandler
{
    public function __construct($em, $entityClass, $formFactory)
    {
        parent::__construct($em, $entityClass, $formFactory);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param array $parameters
     * @return mixed
     */
    public function post($user, array $parameters)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\Client $newClient */
        $newClient = new Client();
        $newClient->setInstance($user->getInstance());
        $form = $this->formFactory->create(new ClientType(), $newClient, array('method' => 'POST'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param \FacturaCohete\BackEndBundle\Entity\Client $client
     * @param array $parameters
     * @return mixed
     */
    public function put($user, $client, array $parameters)
    {

        foreach ($client->getClientContacts() as $clientContact) {
            $this->om->remove($clientContact);
        }
        $client->getClientContacts()->clear();


        $client->setInstance($user->getInstance());
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(new ClientType(), $client, array('method' => 'PUT'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * {@inheritdoc}
     */
    protected function processForm($form)
    {
        if ($form->isValid()) {
            /** @var \FacturaCohete\BackEndBundle\Entity\Client $client */
            $client = $form->getData();
            /** @var \FacturaCohete\BackEndBundle\Entity\ClientContact $clientContact */
            foreach($client->getClientContacts() as $clientContact){
                $clientContact->setClient($client);
                $this->om->persist($clientContact);
            }
            $this->om->persist($client);
            $this->om->flush($client);
            return $client;
        }
        throw new InvalidFormException('Invalid submitted data', $form);
    }

}