<?php

namespace FacturaCohete\BackEndBundle\Handler;

use FacturaCohete\BackEndBundle\Entity\Instance;
use FacturaCohete\BackEndBundle\Entity\Tax;
use FacturaCohete\BackEndBundle\Entity\User;
use FacturaCohete\BackEndBundle\Form\UserType;
use Symfony\Component\Form\Exception;
use FacturaCohete\BackEndBundle\Exception\InvalidFormException;


class UserHandler extends EntityHandler
{
    public function __construct($em, $entityClass, $formFactory)
    {
        parent::__construct($em, $entityClass, $formFactory);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $id
     * @return mixed
     */
    public function get($user, $id)
    {
        return $this->repository->findOneBy(array('id' => $id, 'instance' => $user->getInstance()));
    }

    /**
     * @param $username
     * @param $password
     * @return mixed
     */
    public function getByUsernameAndPassword($username, $password)
    {
        $user = $this->repository->findOneBy(array('username' => $username));
        if ($user and password_verify($password, $user->getPassword())) {
            return $user;
        } else {
            return null;
        }
    }

    /**
     * @param $email
     * @return null|object
     */
    public function getByEmail($email)
    {
        $user = $this->repository->findOneBy(array('email' => $email, 'isActive' => true));
        if ($user) {
            return $user;
        } else {
            return null;
        }
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $page
     * @param $limit
     * @param string $sort
     * @return array
     */
    public function getByPage($user, $page, $limit, $sort = 'id')
    {
        $offset = (null == $page ? 0 : $page - 1) * $limit;
        return $this->repository->findBy(array('instance' => $user->getInstance()), array('id' => 'DESC'), $limit, $offset);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $userSession
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param array $parameters
     * @return mixed
     */
    public function put($userSession, $user, array $parameters)
    {
        $user->setInstance($userSession->getInstance());
        $form = $this->formFactory->create(new UserType(), $user, array('method' => 'PUT'));
        $form->submit($parameters, false);//do not overwrite
        return $this->processForm($form);
    }


    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param array $parameters
     * @return mixed
     */
    public function post($user, array $parameters)
    {
        if (array_key_exists('email', $parameters)) {
            $parameters['username'] = $parameters['email'];
        } else {
            $parameters['username'] = "";
        }
        if (!array_key_exists('isActive', $parameters)) {
            $parameters['isActive'] = true;
        }

        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $newUser = new User();
        $newUser->setInstance($user->getInstance());
        $newUser->setEmailVerified(false);
        $newUser->setToken(md5(uniqid(mt_rand(), true)));
        $user->setTokenExpiration(date_add(new \DateTime(), date_interval_create_from_date_string('1 month')));
        $form = $this->formFactory->create(new UserType(), $newUser, array('method' => 'POST'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * @param array $parameters
     * @throws InvalidFormException
     */
    public function register(array $parameters)
    {
        $email = $parameters['email'];
        /** @var \FacturaCohete\BackEndBundle\Entity\User $userFound */
        $userFound = $this->getByEmail($email);

        if ($userFound) {
            if (!$userFound->getEmailVerified()) {
                $this->om->remove($userFound->getInstance());
                $this->om->flush($userFound->getInstance());
            } else {
                return $userFound;
            }
        }

        $parameters['username'] = $parameters['email'];
        if (!array_key_exists('isActive', $parameters)) {
            $parameters['isActive'] = true;
        }


        /** @var \FacturaCohete\BackEndBundle\Entity\Instance $instance */
        $instance = new Instance();
        if (array_key_exists('organizationName', $parameters)) {
            $instance->setCompanyName($parameters['organizationName']);
        } else {
            $instance->setCompanyName("Cambiar este nombre");
        }
        $instance->setCurrency("CRC");
        $instance->setTaxEnabled(true);
        $instance->setDiscountEnabled(true);
        $instance->setDiscountRate(0);
        $instance->setCompanyType("company");
        $instance->setTaxNumberName("Cédula jurídica");

        /** @var \FacturaCohete\BackEndBundle\Entity\Tax $defaultTax */
        $defaultTax = new Tax();
        $defaultTax->setName("Impuesto de venta");
        $defaultTax->setRate(0.13);
        $defaultTax->setCode("I.V.");
        $defaultTax->setInstance($instance);

        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = new User();
        $user->setInstance($instance);
        $user->setEmailVerified(false);
        $user->setToken(md5(uniqid(mt_rand(), true)));
        $user->setTokenExpiration(date_add(new \DateTime(), date_interval_create_from_date_string('1 month')));
        $form = $this->formFactory->create(new UserType(), $user, array('method' => 'POST'));
        $form->submit($parameters, true);
        if ($form->isValid()) {
            /** @var \FacturaCohete\BackEndBundle\Entity\User $userToSave */
            $userToSave = $form->getData();
            $this->om->persist($instance);
            $this->om->persist($defaultTax);
            $this->om->persist($userToSave);
            $this->om->flush($userToSave);
            return $userToSave;
        } else {
            throw new InvalidFormException('Invalid submitted data', $form);
        }

    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @return mixed
     */
    public function requestPasswordResetToken($user)
    {
        $user->setToken(md5(uniqid(mt_rand(), true)));
        $user->setTokenExpiration(date_add(new \DateTime(), date_interval_create_from_date_string('1 hour')));
        $this->om->persist($user);
        $this->om->flush($user);
        return $user;
    }


    /**
     * @param $token
     * @return null|object
     */
    public function verifyToken($token)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\User $user */
        $user = $this->repository->findOneBy(array('token' => $token));
        if ($user) {
            $user->setEmailVerified(true);//for password reset this will be already verified
            $user->setToken(null);
            $user->setTokenExpiration(null);
            $this->om->persist($user);
            $this->om->flush($user);
            return $user;
        } else {
            return null;
        }
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $newPassword
     * @return User|null
     */
    public function changePassword($user, $newPassword)
    {
        $user->setPassword($newPassword);
        $this->om->persist($user);
        $this->om->flush($user);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param $newEmail
     * @return User|null
     */
    public function changeEmail($user, $newEmail)
    {
        $user->setEmail($newEmail);
        $user->setUsername($newEmail);
        $this->om->persist($user);
        $this->om->flush($user);
    }

}