<?php

namespace FacturaCohete\BackEndBundle\Handler;

use FacturaCohete\BackEndBundle\Entity\Item;
use FacturaCohete\BackEndBundle\Form\ItemType;
use Symfony\Component\Form\Exception;

class ItemHandler extends EntityHandler
{
    public function __construct($em, $entityClass, $formFactory)
    {
        parent::__construct($em, $entityClass, $formFactory);
    }


    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param \FacturaCohete\BackEndBundle\Entity\Item $item
     * @param array $parameters
     * @return mixed
     */
    public function put($user, $item, array $parameters)
    {
        $item->setInstance($user->getInstance());
        $form = $this->formFactory->create(new ItemType(), $item, array('method' => 'PUT'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

    /**
     * @param \FacturaCohete\BackEndBundle\Entity\User $user
     * @param array $parameters
     * @return mixed
     */
    public function post($user, array $parameters)
    {
        /** @var \FacturaCohete\BackEndBundle\Entity\Item $newItem */
        $newItem = new Item();
        $newItem->setInstance($user->getInstance());
        $form = $this->formFactory->create(new ItemType(), $newItem, array('method' => 'POST'));
        $form->submit($parameters, true);
        return $this->processForm($form);
    }

}