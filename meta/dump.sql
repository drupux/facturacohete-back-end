-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: facturacohete
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_user`
--

DROP TABLE IF EXISTS `api_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `instance_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AC64A0BAF85E0677` (`username`),
  KEY `IDX_AC64A0BA3A51721D` (`instance_id`),
  CONSTRAINT `FK_AC64A0BA3A51721D` FOREIGN KEY (`instance_id`) REFERENCES `instance` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_user`
--

LOCK TABLES `api_user` WRITE;
/*!40000 ALTER TABLE `api_user` DISABLE KEYS */;
INSERT INTO `api_user` VALUES (10,'safe','[erased]','email',1,'2014-11-21 03:02:30','2014-11-23 18:44:41',1),(13,'safe2','$2y$12$BpdYD/k5mvO5jua3Dh7GRe9ZpfzYZ5lyu3hF7EVu.Pre6Se3LRMz.','email',1,'2014-11-21 03:15:22','2014-11-21 03:15:22',1),(18,'nuevo2','$2y$12$yUSiUMfHxctJ21T0j0FMq.PRwn/o.w2078S7jXY5WEdfn./UDGrN.','email',1,'2014-11-21 03:30:42','2014-11-21 03:30:42',2),(21,'nuevo3','$2y$12$OmGmZ8bjouYDyBcazPib3.IwGOA8fOcySmqjaLFgc2yo5SzCWCZCO','email',1,'2014-11-23 07:36:24','2014-11-23 07:36:24',2),(23,'nuevo4','$2y$12$k7WuJ8T6wMOS7FpEdyIAF.L3XjJb/OBTRxegWbnfXK.UzTygAY4oW','email',1,'2014-11-23 07:43:39','2014-11-23 07:43:39',1),(25,'manzana','$2y$12$YIDc6vDqtYRA5asp3T/WjeR0LjljpK64zvsl7Pz9iWYgsCSxuX7JK','email@00',1,'2014-11-23 07:44:42','2014-11-23 20:52:13',2);
/*!40000 ALTER TABLE `api_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instance`
--

DROP TABLE IF EXISTS `instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instance`
--

LOCK TABLES `instance` WRITE;
/*!40000 ALTER TABLE `instance` DISABLE KEYS */;
INSERT INTO `instance` VALUES (1,'fundacion neotrópica','CRC','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'drupux','USD','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `track_inventory` tinyint(1) NOT NULL,
  `inventory` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `instance_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'panta','des',3434,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',1),(3,'pantalon','caro',34234,0,34,'2015-11-23 05:48:57','2015-11-23 05:48:57',2),(4,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:32:13','2014-11-23 17:32:13',1),(5,'dress3','vestido largo3',3333,0,336666,'2014-11-23 17:33:06','2014-11-23 20:57:46',2),(6,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:44:51','2014-11-23 17:44:51',1),(7,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:44:53','2014-11-23 17:44:53',1),(8,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:51:44','2014-11-23 17:51:44',1),(9,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:51:56','2014-11-23 17:51:56',1),(10,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 17:59:31','2014-11-23 17:59:31',1),(11,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 18:26:01','2014-11-23 18:26:01',1),(12,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 18:26:51','2014-11-23 18:26:51',1),(13,'Camisa de vestir','Muy a la moda',340.34,1,233,'2014-11-23 18:26:55','2014-11-23 18:26:55',1);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-23 22:20:32
